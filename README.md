# 3D Tic Tac Toe 4x4x4 Board

Programa escrito para Turbo Pascal que permite a dos personas jugar una versión tridimensional de 4x4x4 del famoso juego del gato (tic-tac-toe), lleva el conteo de partidas jugadas y ganadas por jugador.

![Image Tablero de juego](https://gitlab.com/oscarislasleyva/3D-Tic-Tac-Toe-4x4x4-board/-/raw/main/TableroGato3D.png)

<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
Para ver un video del final de una partida por favor ve a [Gato 3D](https://www.youtube.com/watch?v=GQQGFCrLOV4).

# Pasos para ejecutar el código en Windows 10

- Tener instalado [Git BASH](https://gitforwindows.org/).
- En la consola de _Git BASH_ ubicarte una carpeta fácil de recordar como la raíz de la unidad C:\\\, probablemente en el prompt lo verás similar a `user@computer MSYS /c $` y clona el repositorio en un carpeta local llamada "**g3d**" 

   `git clone https://gitlab.com/oscarislasleyva/3D-Tic-Tac-Toe-4x4x4-board.git g3d`
- Tener instalado DOSBox y Turbo Pascal, en SourceForge existe un paquete que incluye ambos: [DOSBox + Turbo Pascal](https://sourceforge.net/projects/turbopascal-wdb/)
- Ejecutar el IDE de Turbo Pascal en DOSBox. Cambiar el directorio de trabajo por default del IDE al de la copia local del repositorio usando las opción del menú `File > Change dir ...` y navegando hasta encontrar el directorio "**g3d**".
- Abrir el archivo fuente `GATO3DV1.PAS` con la opción de menú `File > Open ...`
- Correr el programa con la opción del menú `Run` o presionando las teclas `Ctrl+F9`



## Autors

- [@oscarislasleyva](https://gitlab.com/oscarislasleyva)
